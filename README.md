# Приложение проверки наличие скидки товара на pepper

## Конфигурация

### Авторизация

| Параметр      | Описание                   |
| ------------- | -------------------------- |
| AUTH_USERNAME | Логин для Http Basic Auth  |
| AUTH_PASSWORD | Пароль для Http Basic Auth |

### Настройка Flask

| Параметр    | Описание                          |
| ----------- | --------------------------------- |
| FLASK_HOST  | IP адрес сайта                    |
| FLASK_PORT  | Порт сервера                      |
| FLASK_DEBUG | Вкл./Выкл. встроенный debug flask |

### База данных

| Параметр    | Описание                 |
| ----------- | ------------------------ |
| DB_HOST     | IP адрес СУБД            |
| DB_PORT     | Порт СУБД                |
| DB_DATABASE | База данных              |
| DB_USERNAME | Пользователь базы данных |
| DB_PASSWORD | Пароль пользователя      |

## API

✅ - Готово
⚙️ - В разработке
❌ - Не реализованно

| Метод HTTP | Запрос                           | Функционал                                  | Работоспособность |
| ---------- | -------------------------------- | ------------------------------------------- | ----------------- |
| POST       | /api/add-mon-product/?product=   | Добавляет товар, который будет мониториться | ✅                |
| DELETE     | /api/delete-product/?product-id= | Удалить товар из мониторинга                | ⚙️                |
| GET        | /api/check-products/             | Проверить товары на наличие скидок          | ✅                |
| GET        | /api/active-product/             | Список активных товаров                     | ❌                |
| PUT        | /api/resume-product/<product_id> | Возобновить монитор                         | ❌                |