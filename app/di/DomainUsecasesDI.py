from re import A
from dependency_injector import containers, providers

from app.domain.usecase import AddProductMonitoringUseCase
from app.domain.usecase import GetAllProductMonitoringUseCase
from app.domain.usecase import InitDBTableUsecase
from app.domain.usecase import GetPageUseCase
from app.domain.usecase import GetProductDataUseCase
from app.domain.usecase import GetNumberDiscountsUseCase
from app.domain.usecase import GetFullNumberProductPagesUseCase
from app.domain.usecase import DeleteActiveProductByProductMonitoringIdUseCase
from app.domain.usecase import AddActiveProductUseCase
from app.domain.usecase import GetAllActiveProductsUseCase

from app.data.repository import AddProductMonitoringRepositoryImpl
from app.data.repository import GetAllProductMonitoringRepositoryImpl
from app.data.repository import InitDBRepositoryImpl
from app.data.repository import DeleteActiveProductByProductMonitoringIdRepositoryImpl
from app.data.repository import AddActiveProductRepositoryImpl
from app.data.repository import GetAllActiveProductsRepositoryImpl

from app.data.storage.database import AddProductMonitoring
from app.data.storage.database import GetAllProductMonitoringStorage
from app.data.storage.database import DatabaseInitStorage
from app.data.storage.database import DeleteActiveProductByProductMonitoringIdStorage
from app.data.storage.database import AddActiveProductStorage
from app.data.storage.database import GetAllActiveProductsStorage


class DomainUsecasesDI(containers.DeclarativeContainer):

    config = providers.Configuration()

    __database_init_storage = providers.Singleton(
        DatabaseInitStorage,
    )

    __init_db_repository_impl = providers.Singleton(
        InitDBRepositoryImpl,
        init_db_storage=__database_init_storage,
    )

    __get_all_product_monitoring_storage = providers.Singleton(
        GetAllProductMonitoringStorage,
    )

    __get_all_product_monitoring_repository_impl = providers.Singleton(
        GetAllProductMonitoringRepositoryImpl,
        get_all_product_monitoring_db_storage=__get_all_product_monitoring_storage
    )

    __add_product_monitoring = providers.Singleton(
        AddProductMonitoring,
    )

    __add_product_monitoring_repository_impl = providers.Singleton(
        AddProductMonitoringRepositoryImpl,
        add_product_monitoring_db_storage=__add_product_monitoring,
    )

    __delete_active_product_by_product_monitoring_id_storage = providers.Singleton(
        DeleteActiveProductByProductMonitoringIdStorage,
    )

    __delete_active_product_by_product_monitoring_id_repository_impl = providers.Singleton(
        DeleteActiveProductByProductMonitoringIdRepositoryImpl,
        delete_active_product_by_product_monitoring_id_db_storage=__delete_active_product_by_product_monitoring_id_storage,
    )

    __add_active_product_storage = providers.Singleton(
        AddActiveProductStorage,
    )

    __add_active_product_repository_impl = providers.Singleton(
        AddActiveProductRepositoryImpl,
        add_active_product_db_storage=__add_active_product_storage,
    )

    __get_all_active_products_storage = providers.Singleton(
        GetAllActiveProductsStorage,
    )

    __get_all_active_products_repository_impl = providers.Singleton(
        GetAllActiveProductsRepositoryImpl,
        get_all_active_products_db_storage=__get_all_active_products_storage
    )

    get_all_active_products_use_case = providers.Singleton(
        GetAllActiveProductsUseCase,
        get_all_active_products_repository=__get_all_active_products_repository_impl
    )

    add_active_product_use_case = providers.Singleton(
        AddActiveProductUseCase,
        add_active_product_repository=__add_active_product_repository_impl
    )

    add_product_monitoring_use_case = providers.Singleton(
        AddProductMonitoringUseCase,
        add_product_monitoringRepositor=__add_product_monitoring_repository_impl,
    )

    get_all_product_monitoring_use_case = providers.Singleton(
        GetAllProductMonitoringUseCase,
        get_all_product_monitoring_repository=__get_all_product_monitoring_repository_impl
    )

    delete_active_product_by_product_monitoring_id_use_case = providers.Singleton(
        DeleteActiveProductByProductMonitoringIdUseCase,
        delete_active_product_by_product_monitoring_id_repository=__delete_active_product_by_product_monitoring_id_repository_impl,
    )

    get_page_use_case = providers.Singleton(GetPageUseCase)
    get_product_data_use_case = providers.Singleton(GetProductDataUseCase)
    get_number_discounts_use_case = providers.Singleton(
        GetNumberDiscountsUseCase)
    get_full_number_product_pages_use_case = providers.Singleton(
        GetFullNumberProductPagesUseCase)

    init_db = providers.Singleton(
        InitDBTableUsecase,
        init_db_repository=__init_db_repository_impl,
    )
