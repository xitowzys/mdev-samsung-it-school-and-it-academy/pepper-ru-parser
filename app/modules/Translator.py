from transliterate import translit

class Translator():
    def execute(self, lang: str, text: str) -> str:
        return translit(f"{text}", lang, reversed=True)