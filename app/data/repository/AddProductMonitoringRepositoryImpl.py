from app.data.storage.IAddProductMonitoringDBStorage import IAddProductMonitoringDBStorage
from app.domain.repository.IAddProductMonitoringRepository import IAddProductMonitoringRepository
from app.domain.models.ProductName import ProductName as domainModelsProductName
from app.data.storage.models.ProductName import ProductName as storageModelsProductName

class AddProductMonitoringRepositoryImpl(IAddProductMonitoringRepository):

    def __init__(self, add_product_monitoring_db_storage: IAddProductMonitoringDBStorage):
        self.add_product_monitoring_db_storage: IAddProductMonitoringDBStorage = add_product_monitoring_db_storage

    def add(self, product_name: domainModelsProductName) -> int:
        storage_models_product_name: storageModelsProductName = storageModelsProductName(product_name.title)
        return self.add_product_monitoring_db_storage.add(storage_models_product_name)

