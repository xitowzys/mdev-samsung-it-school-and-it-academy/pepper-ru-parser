import sqlalchemy as sa
from app.data.storage.database.SQLAlchemy.declarative_base import Base
from sqlalchemy.dialects.mysql import INTEGER, BOOLEAN
from sqlalchemy.orm import relationship
from dataclasses import dataclass
@dataclass
class ProductMonitoring(Base):
    __tablename__ = 'product_monitoring'

    id = sa.Column(INTEGER(unsigned=True),
                   primary_key=True, autoincrement=True)
    title = sa.Column(sa.VARCHAR(length=255), nullable=False)
    is_monitoring = sa.Column(BOOLEAN, nullable=False, default=1)
    is_deleted = sa.Column(BOOLEAN, nullable=False, default=0)

    active_products_id = relationship("ActiveProducts", backref="product_monitoring")