from abc import ABC, abstractmethod

class IGetAllProductMonitoringDBStorage(ABC):

    @abstractmethod
    def get(self) -> int:
        pass