from abc import ABC, abstractmethod


class IGetAllActiveProductsRepository(ABC):

    @abstractmethod
    def get(self) -> dict:
        pass
