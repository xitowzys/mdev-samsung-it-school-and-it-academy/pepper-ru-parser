from abc import ABC, abstractmethod


class IGetAllProductMonitoringRepository(ABC):

    @abstractmethod
    def get(self) -> dict:
        pass
