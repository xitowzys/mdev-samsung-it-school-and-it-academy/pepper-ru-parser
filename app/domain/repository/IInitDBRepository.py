from abc import ABC, abstractmethod


class IInitDBRepository(ABC):
    """
    Interface
    ----------
    The interface used for database initialization
    Methods
    -------
    init_tables()
        Initialize tables
    """

    @abstractmethod
    def init_tables(self) -> bool:
        pass
