from dataclasses import dataclass


@dataclass
class ProductName:
    title: str