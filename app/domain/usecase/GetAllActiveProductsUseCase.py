from loguru import logger as log
from app.domain.repository.IGetAllActiveProductsRepository import IGetAllActiveProductsRepository

class GetAllActiveProductsUseCase():
    def __init__(self, get_all_active_products_repository: IGetAllActiveProductsRepository):
        self.get_all_active_products_repository: IGetAllActiveProductsRepository = get_all_active_products_repository

    def execute(self) -> dict:
        log.info("Получение всех активных товаров")
        return self.get_all_active_products_repository.get()
