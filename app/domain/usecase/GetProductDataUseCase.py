from bs4 import BeautifulSoup


class GetProductDataUseCase():
    async def execute(self, page) -> list:
        result = []
        soup = BeautifulSoup(page, 'lxml')

        products_data_article = soup.find_all("article", {"class": "thread--deal"})

        for t in products_data_article:
            if not t.decomposed:
                res = [
                    int(t.get('id').split("_")[1]),
                    t.find("a", class_="thread-title--list").get('title'),
                    t.find("a", class_="thread-title--list").get('href')
                ]

                result.append(res)

        return result
