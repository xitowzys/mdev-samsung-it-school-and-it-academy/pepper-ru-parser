from bs4 import BeautifulSoup


class GetNumberDiscountsUseCase():
    def execute(self, page) -> int:

        soup = BeautifulSoup(page, "lxml")

        number_discounts_all_span = soup.find(
            "li", {"class": "js-subNavItem-search"}).find("span", {"class": "js-subNavItem-count"})

        test = number_discounts_all_span.text

        return int(number_discounts_all_span.text)
