from loguru import logger as log
from app.domain.repository.IGetAllProductMonitoringRepository import IGetAllProductMonitoringRepository

class GetAllProductMonitoringUseCase():
    def __init__(self, get_all_product_monitoring_repository: IGetAllProductMonitoringRepository):
        self.get_all_product_monitoring_repository: IGetAllProductMonitoringRepository = get_all_product_monitoring_repository

    def execute(self) -> dict:
        log.info("Получение всех товаров с статусом мониторинг")
        return self.get_all_product_monitoring_repository.get()
