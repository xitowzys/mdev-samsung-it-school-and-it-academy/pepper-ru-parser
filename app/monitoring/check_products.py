import time
import requests
import asyncio
from loguru import logger as log
from config import FLASK_HOST, FLASK_PORT, TIME_MONITORING
from app.di import DomainUsecasesDI
from app.domain.models.ProductMonitoringId import ProductMonitoringId
from app.domain.models.ActiveProduct import ActiveProduct

usecases = DomainUsecasesDI()


async def get_pages_count(products: dict) -> list:
    tasks, result = [], []

    for product in products:
        tasks.append(asyncio.create_task(
            usecases.get_full_number_product_pages_use_case().execute(product[1])))

    tasks_result = await asyncio.gather(*tasks)

    for task_result in tasks_result:
        result.append(task_result[0])

    return result


async def get_pages_html(products: dict, pages_count: list):
    tasks, result = [], []

    for i, product in enumerate(products):
        for j in range(1, pages_count[i] + 1):
            tasks.append(asyncio.create_task(
                usecases.get_page_use_case().execute(product[1], j)))

    tasks_result = await asyncio.gather(*tasks)

    for task_result in tasks_result:
        result.append(task_result[0])

    return result



async def parse_products():

    products = usecases.get_all_product_monitoring_use_case().execute()

    log.info(f"(Мониторинг)➤ Проверка товаров {products}")

    result = {}
    tasks = []

    pages_count = await get_pages_count(products=products)
    data = await get_pages_html(products=products, pages_count=pages_count)

    tasks.clear()
    for i_data in data:
        tasks.append(asyncio.create_task(
            usecases.get_product_data_use_case().execute(i_data)))

    data = await asyncio.gather(*tasks)

    start_range = 0
    stop_range = 0

    for i, page_count in enumerate(pages_count):
        joj = []
        stop_range += page_count

        for j in range(start_range, stop_range):
            joj += data[j]
        
        start_range += page_count
        result[products[i][0]] = joj

    for id in result.keys():
        product_id = ProductMonitoringId(id)
        usecases.delete_active_product_by_product_monitoring_id_use_case().execute(product_id)

    for id in result.keys():
        for product in result[id]:
            active_product = ActiveProduct(id, product[0], product[1], product[2])
            usecases.add_active_product_use_case().execute(active_product)

    log.success("(Мониторинг)➤ Проверка завершена")


async def check_product():

    # while True:
    #     response = requests.get(f'http://{FLASK_HOST}:{FLASK_PORT}')

    #     if response.status_code == 404:
    #         while True:
    #             await parse_products()
    #             time.sleep(TIME_MONITORING)
    #     else:
    #         time.sleep(5)

    while True:
        await parse_products()
        time.sleep(TIME_MONITORING)