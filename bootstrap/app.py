import asyncio
import threading
import api.init_api as initAPI

from config import MONITORING
from app.di import DomainUsecasesDI
from app.monitoring import check_product


usecases = DomainUsecasesDI()


def init_database() -> None:
    usecases.init_db().execute()


def start_app():

    init_database()
    initAPI.init()

    if MONITORING:
        loop = asyncio.get_event_loop()
        # thread_1 = threading.Thread(target=asyncio.run(check_product()))
        # thread_1.start()

        try:
            tasks = asyncio.run(check_product())
        except KeyboardInterrupt as e:
            print("Caught keyboard interrupt. Canceling tasks...")
            tasks.cancel()
            loop.run_forever()
            tasks.exception()
        finally:
            loop.close()


        # asyncio.run(check_product())
